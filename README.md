# Development Checklist


## Device
----

1.	Can the app be installed on the device?
2.	Does the app behave as designed / desired if
    *    there is an incoming call?
    *    the user accept (answer) the call?
    *    the user cancel (reject) the call?
    *    there is an incoming SMS?
    *    designed/desired if the charger is connected?
    *    designed/desired if the charger is disconnected?
    *    the device goes to sleeping mode
    *    the device resumes from sleeping mode
    *    the device resumes from lock screen?
    *    the device is tilted (device motions)?
    *    the device is rotated (portrait or landscape orientation )?
    *    the device is shaken?
    *    a local message is coming from another app (think of: calendar reminders, to-do task etc.).
    *    a push message is coming from another app (think of: Facebook mentions, WhatsApp message, etc).
    *    the **Battery low** message is pushed
    *    the sound on the device is turned off?
    *    there is a low storage ?
    *    there is NO storage ?
    *    the user accept (answer) the call?

3.	Does the app interact with the GPS sensor correctly (switch on/off, retrieve GPS data)?
4.	Verify that unmapped keys are not working on any screen of application.
5.	In case there’s a true **back** button available on the device does the **back** button take the user to the previous screen?
6.	In case there’s a true **menu** button available on the device, does the menu button show the app’s menu?
7.	In case there’s a true **home** button available on the device, does the home button get the user back to the home screen of the device?
8.	does the app works as expected when the user use home button jump ?
9.	In case there’s a true **search** button available on the device, does this get the user to some form of search within the app?
10.	Can the app be found in the app store? (Check after go-live)
11.	Can the app switch to different apps on the device through multitasking as designed/desired?
12.	If performance is slow, indicate a progress status icon (*Loading…*), preferably with specific message.
13.	Can the app be uninstalled from the device?
14.	Does the application function as expected after re-installation?


## Network 
----

1.	Does the app behave according to specification if 
    *    connected to the internet through Wi-Fi?
    *    connected to the internet through 4G (LTE)?
    *    connected to the internet through 3G?
    *    connected to the internet through 2G?
    *    the app is out of network reach (No connection)?

2.	Does the app resume working when it gets back into network reach from outside reach of the network?
3.	Update transactions are processed correctly after re-establishing connection.
4.	What happens if the app switches between networks (Wi-Fi, 3G, 2G)
5.	Does the app use standard network ports (Mail: 25, 143, 465, 993 or 995 HTTP: 80 or 443 SFTP: 22) to connect to remote services, as some providers block certain ports.
6.	Does the app behave as designed/desired if the device is in airplane mode?


## App
----

1.	Has the app been tested on different type of devices and different versions of OS?
2.	Has the app been tested on different types of Oss (Android / IOS) ?
3.	Verify that the application works successfully after automatic update
4.	Stability check: if the app has a list (for instance of pictures) in it, try scrolling through it at high speed.
5.	Stability check: if the app has a list (for instance of pictures) in it, try scrolling to before the first picture or behind the last picture.
6.	Integration: does the app connect correctly to the different social networks (LinkedIn, twitter, facebook, etc).
7.	The app does not interfere with other apps when in background/multitasking mode (using GPS, playing music, etc.).
8.	Can the user print from the app (if applicable)
9.	The search option in the app displays relevant results
10.	Verify most common gestures used to control the app.
For more info please visit 
 [Google Gestures](https://material.google.com/patterns/gestures.html)
11.	What happens if you select different options at the same time (undesired multitouch, for example – select two contacts from the phone book at the same time).
12.	Does the app limit or clean the amount of cached data.
13.	Reloading of data from remote service has been properly designed to prevent performance issues at server-side. (manual reloading of data can reduce the amount of server calls)
14.	Does the app go to sleep mode when running in the background (prevent battery drain)
15.	Verify that unmapped keys are not working on any screen of application.
16.	Verify that there is no UI or data truncation issue when we use the mobile app with different languages (or say, non-English language).
17.	Verify that time zone changes are handled gracefully for your mobile application.
18.	Text is translated.
19.	Translations meet the standards of native speakers with respect to grammar and accuracy of terminology.
20.	Dialog boxes are properly resized and dialog text is hyphenated according to the rules of the user interface language.
21.	Translated dialog boxes, status bars, toolbars, and menus fit on the screen at different resolutions. They do not wrap and are not cut off.
22.	Verify that the default input language is the same as the logged in language 
23.	Verify that the application fields accepts all the available languages , for example if we logged in English and we need to write in other language it should be done correctly by switching the language from the device keyboard .
24.	Battery usage– It’s important to keep a track of battery consumption while running application on the mobile devices.
25.	Speed of the application- the response time on different devices, with different memory parameters, with different network types etc.
26.	Modules that have empty data should leads to displays a message.
27.	Default image should be added to the modules contents with no defined image.
28.	Images with large size should be loaded and navigating properly.
29.	In case we have larger number of content in one module then the application should work properly (Quickly) and a next – previous buttons should displays 
30.	Services testing– Testing the services of the application online and offline.
31.	Low level resource testing: Testing of memory usage, auto deletion of temporary files, local database growing issues known as low level resource testing.


## Store
----

1.	The app should not access information on the device outside the app without the user’s permission (for instance, copying the address book or getting information from other apps).
2.	You cannot mention other app platforms in your app (for instance: **Also available on android!**)
3.	An app cannot use location services of the device without asking permission.
4.	All url’s in the app code should be fully functional
5.	An app cannot use push notifications without user consent.
6.	Push notification can’t send personal information.


## Security
----

1.	Is your application storing payment information or credit card details?
2.	Does your application use secure network protocols (In case we have payments )?
3.	Can they be switched to insecure ones?
4.	Does the application ask for more permissions than it needs?
5.	Does your application use certificates?
6.	Does your application require a user to be authenticated before they are allowed to access their data?
7.	Is there a maximum number of login attempts before they are locked out?

